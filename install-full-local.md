# Installer Communecter sur Ubuntu avec php5.6 et php7.4
## Prérequis
- Installer apache2
sudo apt install apache2
- Installer docker
https://docs.docker.com/engine/install/ubuntu/
- Installer libapache2-mod-fcgid
    `sudo apt install libapache2-mod-fcgid`
- Installer php5.6-fpm et php7.4-fpm
    - `sudo apt install software-properties-common`
    - `sudo add-apt-repository ppa:ondrej/php`
    - `sudo apt update -y`
    - `sudo apt install php5.6 php5.6-fpm -y`
    - `sudo apt install php7.4 php7.4-fpm -y`
- Activer les modules requis pour la configuration de plusieurs versions de PHP avec Apache
    - `sudo a2enmod actions fcgid alias proxy_fcgi`
- Installation des modules requis
    - `sudo apt install php5.6 php5.6-fpm php5.6-curl php5.6-gd php5.6-mbstring php5.6-mcrypt php5.6-mongo php5.6-mongodb -y`
    - `sudo apt install php7.4 php7.4-fpm php7.4-curl php7.4-gd php7.4-mbstring php7.4-mongodb -y`
- Installer mongo sur docker
    - Mongo3.4
        - `docker pull mongo:3.4`
        - `mkdir ~/data && mkdir ~/data/mongo34`
        - `docker run -d -v ~/data/mongo34:/data/db -p 34017:27017 --name mongo34 mongo:3.4`
    - Mongo4.2
        - `docker pull mongo:4.2`
        - `mkdir ~/data && mkdir ~/data/mongo42`
        - `docker run -d -v ~/data/mongo42:/data/db -p 42017:27017 --name mongo42 mongo:4.2`
## Installation
- Copie du projet
    - `mkdir ~/dev && ~/dev/communecter-php56 && ~/dev/communecter-php74`
    - Copier les codes sources dans chacune de repertoire suivantes: `~/dev/communecter-php56` et `~/dev/communecter-php74`
- Changement des branches pour php-7.4
    - `cd ~/dev/communecter-php74/pixelhumain/`
    - `git checkout yii1-yii2-php7 && git pull`
    - `cd ~/dev/communecter-php74/modules/citizenToolKit/`
    - `git checkout yii1-yii2-php7-flysystem && git pull`
    - `cd ~/dev/communecter-php74/modules/co2/`
    - `git checkout yii1-yii2-php7-flysytem && git pull`
    - `git checkout ~/dev/communecter-php74/modules/costum/`
    - `git checkout mongo-3.6 && git pull`
    - `cd ~/dev/communecter-php74/modules/survey/`
    - `git checkout mongo-3.6 && git pull`
    - `cd ~/dev/communecter-php74/modules/news/`
    - `git checkout mongo-3.6 && git pull`
    - `cd ~/dev/communecter-php74/modules/interop/`
    - `git checkout mongo-3.6 && git pull`
- Installation des dependenses via composer
    - `cd ~/dev/communecter-php74/pixelhumain/ph`
    - `~/dev/communecter-php74/pixelhumain/ph/composer.phar install`
- création droit en ecriture
    - `sudo chmod -R 777 ~/dev/{communecter-php56/pixelhumain/ph/assets,communecter-php74/pixelhumain/ph/assets}`
    - `sudo chmod -R 777 ~/dev/{communecter-php56/pixelhumain/ph/upload,communecter-php74/pixelhumain/ph/upload}`
    - `sudo chmod -R 777 ~/dev/{communecter-php56/pixelhumain/ph/protected/runtime,communecter-php74/pixelhumain/ph/protected/runtime}`
    - `sudo chown -R $USER:www-data ~/dev/communecter-php56/pixelhumain`
    - `sudo chown -R $USER:www-data ~/dev/communecter-php56/modules`
    - `sudo chown -R $USER:www-data ~/dev/communecter-php74/pixelhumain`
    - `sudo chown -R $USER:www-data ~/dev/communecter-php74/modules`
- Création de lien symbolique
    - `cd /var/www/html/`
    - `sudo ln -s ~/dev/communecter-php56 communecter-php56`
    - `sudo ln -s ~/dev/communecter-php74 communecter-php74`
- Configuration dbconfig.php
    - `nano ~/dev/communecter-php56/pixelhumain/ph/protected/config/dbconfig.php`
    - ```php
        <?php
            $dbconfig = array(
                'class' => 'mongoYii.EMongoClient',
                'server' => 'mongodb://127.0.0.1:34017/',
                'db' => 'pixelhumain', 
            );
        ?>
        ```
    - `nano ~/dev/communecter-php74/pixelhumain/ph/protected/config/dbconfig.php`
    - ```php
        <?php
            $dbconfig = array(
                'class' => 'mongoYii.EMongoClient',
                'server' => 'mongodb://127.0.0.1:42017/',
                'db' => 'pixelhumain', 
            );
        ?>
        ```
- Configuration vhosts
    - `sudo nano /etc/hosts`
    
    - ```js
        127.0.0.1   communecter56-dev
        127.0.0.1   communecter74-dev
        ```  
    - `sudo touch /etc/apache2/sites-available/communecter56-dev.conf`

    - `sudo nano /etc/apache2/sites-available/communecter56-dev.conf`

    - ```xml
        <VirtualHost *:80>
            ServerName communecter56-dev
            ServerAdmin webmaster@domain.ext
            DocumentRoot "/var/www/html/communecter-php56/pixelhumain/ph"
            ErrorLog ${APACHE_LOG_DIR}/error.log
            CustomLog ${APACHE_LOG_DIR}/access.log combined
            #Alias /ph "/var/www/html/communecter-php56/pixelhumain/ph"
            <Directory "/var/www/html/communecter-php56/pixelhumain/ph">
            Options -Indexes +FollowSymLinks +MultiViews
                AllowOverride all
                Order deny,allow
                Allow from all
                #deny from all
                <IfModule mod_rewrite.c>
                    Options +FollowSymLinks
                    IndexIgnore /
                    RewriteEngine on
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteCond %{REQUEST_FILENAME} !-d
                    RewriteCond $1 !^(index\.php|assets|robots\.txt)
                    RewriteRule ^(.*)$ /index.php/$1 [L]
                </IfModule>
            </Directory>
            <FilesMatch \.php$>
                # Apache 2.4.10+ can proxy to unix socket
                SetHandler "proxy:unix:/var/run/php/php5.6-fpm.sock|fcgi://localhost"
            </FilesMatch>
        </VirtualHost>
        ```
    - `sudo a2ensite communecter56-dev`

    - `sudo touch /etc/apache2/sites-available/communecter74-dev.conf`

    - `sudo nano /etc/apache2/sites-available/communecter74-dev.conf`

    - ```xml
        <VirtualHost *:80>
            ServerName communecter74-dev
            ServerAdmin webmaster@domain.ext
            DocumentRoot "/var/www/html/communecter-php74/pixelhumain/ph"
            ErrorLog ${APACHE_LOG_DIR}/error.log
            CustomLog ${APACHE_LOG_DIR}/access.log combined
            #Alias /ph "/var/www/html/communecter-php74/pixelhumain/ph"
            <Directory "/var/www/html/communecter-php74/pixelhumain/ph">
            Options -Indexes +FollowSymLinks +MultiViews
                AllowOverride all
                Order deny,allow
                Allow from all
                #deny from all
                <IfModule mod_rewrite.c>
                    Options +FollowSymLinks
                    IndexIgnore /
                    RewriteEngine on
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteCond %{REQUEST_FILENAME} !-d
                    RewriteCond $1 !^(index\.php|assets|robots\.txt)
                    RewriteRule ^(.*)$ /index.php/$1 [L]
                </IfModule>
            </Directory>
            <FilesMatch \.php$>
                # Apache 2.4.10+ can proxy to unix socket
                SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost"
            </FilesMatch>
        </VirtualHost>
        ```
    
    - `sudo a2ensite communecter74-dev`

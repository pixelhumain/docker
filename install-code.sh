#!/bin/bash

#verifier ubuntu
#verifier docker
if ! docker info >/dev/null 2>&1; then
    echo "Docker pas présent ou démarer"
    exit 1
fi
#verifier docker-compose
if ! docker-compose version >/dev/null 2>&1; then
    echo "Docker-compose pas présent"
    exit 1
fi

cd ~/

if [ -d "${PWD}/pixelhumain-php7/" ]
then
echo "pixelhumain-php7 déja présent"
else
  git clone https://gitlab.adullact.net/pixelhumain/docker.git ~/pixelhumain-php7/
fi


cd ~/pixelhumain-php7/

git checkout php7

if [ -d "${PWD}/code/" ]
then
echo "code déja présent"
else
  mkdir ~/pixelhumain-php7/code/
fi


docker-compose -f docker-compose-apache.yml build

cd ~/pixelhumain-php7/code/

if [ -d "${PWD}/pixelhumain/" ]
then
echo "pixelhumain déja présent"
else
  git clone https://gitlab.adullact.net/pixelhumain/pixelhumain.git pixelhumain
fi


mkdir modules
cd modules

git clone https://gitlab.adullact.net/pixelhumain/communecter.git communecter
git clone https://gitlab.adullact.net/pixelhumain/citizenToolkit.git citizenToolKit
git clone https://gitlab.adullact.net/pixelhumain/co2.git co2
git clone https://gitlab.adullact.net/pixelhumain/api.git api
git clone https://gitlab.adullact.net/pixelhumain/dda.git dda
git clone https://gitlab.adullact.net/pixelhumain/news.git news
git clone https://gitlab.adullact.net/pixelhumain/interop.git interop
git clone https://gitlab.adullact.net/pixelhumain/graph.git graph
git clone https://gitlab.adullact.net/pixelhumain/eco.git eco
git clone https://gitlab.adullact.net/pixelhumain/chat.git chat
git clone https://gitlab.adullact.net/pixelhumain/survey.git survey
git clone https://gitlab.adullact.net/pixelhumain/map.git map
git clone https://gitlab.adullact.net/pixelhumain/places.git places
git clone https://gitlab.adullact.net/pixelhumain/cotools.git cotools
git clone https://gitlab.adullact.net/pixelhumain/costum.git costum

cd ~/pixelhumain-php7/

#lancer les services

docker-compose -f docker-compose-apache.yml up -d

#lancer sur le service front la commande pour installer les packages depuis composer

docker-compose -f docker-compose-apache.yml exec -T front composer config -g "secure-http" false -d /code/pixelhumain/ph

docker-compose -f docker-compose-apache.yml exec -T front composer install -d /code/pixelhumain/ph

# Setup configuration for MongoDB
if [ -f "${PWD}/code/pixelhumain/ph/protected/config/dbconfig.php" ]
then
echo "configuration mongodb déja présente : $PWD/code/pixelhumain/ph/protected/config/dbconfig.php"
else
  cat > "${PWD}/code/pixelhumain/ph/protected/config/dbconfig.php" <<EOF
  <?php

  \$dbconfig = array(
      'class' => 'mongoYii.EMongoClient',
      'server' => 'mongodb://mongo:27017/',
      'db' => 'pixelhumain',
  );
  \$dbconfigtest = array(
      'class' => 'mongoYii.EMongoClient',
      'server' => 'mongodb://mongo:27017/',
      'db' => 'pixelhumaintest',
  );
EOF
fi

docker-compose -f docker-compose-apache.yml exec -T front mkdir -vp /code/pixelhumain/ph/{assets,upload,protected/runtime}

docker-compose -f docker-compose-apache.yml exec -T front chmod -R 777 /code/pixelhumain/ph/assets
docker-compose -f docker-compose-apache.yml exec -T front chmod -R 777 /code/pixelhumain/ph/upload
docker-compose -f docker-compose-apache.yml exec -T front chmod -R 777 /code/pixelhumain/ph/protected/runtime

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.createUser({ user: 'pixelhumain', pwd: 'pixelhumain', roles: [ { role: "readWrite", db: "pixelhumain" } ] });
EOF

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumaintest <<EOF
db.createUser({ user: 'pixelhumain', pwd: 'pixelhumain', roles: [ { role: "readWrite", db: "pixelhumain" } ] });
EOF

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.lists.dropIndexes();
db.lists.remove({});
EOF

docker-compose -f docker-compose-apache.yml exec -T mongo mongoimport --host mongo --db pixelhumain --collection lists "/code/modules/co2/data/lists.json" --jsonArray

#Test cities.json
if [ -f "${PWD}/code/modules/co2/data/cities.json" ];then
 rm "${PWD}/code/modules/co2/data/cities.json"
fi

if [ -f "${PWD}/code/modules/co2/data/cities.json.zip" ];then

unzip "${PWD}/code/modules/co2/data/cities.json.zip" -d "${PWD}/code/modules/co2/data/"

#delete cities and delete all index cities
docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.cities.dropIndexes();
db.cities.remove({});
EOF

echo "Import cities data..."
#import cities
docker-compose -f docker-compose-apache.yml exec -T mongo mongoimport --host mongo --db pixelhumain --collection cities "/code/modules/co2/data/cities.json"
fi

#Test zones.json
if [ -f "${PWD}/code/modules/co2/data/zones.json" ];then
 rm "${PWD}/code/modules/co2/data/zones.json"
fi

if [ -f "${PWD}/code/modules/co2/data/zones.json.zip" ];then

unzip "${PWD}/code/modules/co2/data/zones.json.zip" -d "${PWD}/code/modules/co2/data/"

#delete cities and delete all index cities
docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.zones.dropIndexes();
db.zones.remove({});
EOF

echo "Import zones data..."
#import zones
docker-compose -f docker-compose-apache.yml exec -T mongo mongoimport --host mongo --db pixelhumain --collection zones "/code/modules/co2/data/zones.json"
fi

unzip ~/pixelhumain-php7/code/modules/co2/data/translate.json.zip -d ~/pixelhumain-php7/code/modules/co2/data/

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.translate.dropIndexes();
db.translate.remove({});
EOF


docker-compose -f docker-compose-apache.yml exec -T mongo mongoimport --host mongo --db pixelhumain --collection translate "/code/modules/co2/data/translate.json"

docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.createCollection("applications")
db.applications.insert({     "_id" : ObjectId("59f1920bc30f30536124355d"),     "name" : "DEV Config",     "key" : "devParams",     "mangoPay" : {         "ClientId" : "communecter",         "ClientPassword" : "xxxx",         "TemporaryFolder" : "../../tmp"     } } )
db.applications.insert({     "_id" : ObjectId("59f1920bc30f30536124355e"),     "name" : "PROD Config",     "key" : "prodParams",     "mangoPay" : {         "ClientId" : "communecter",         "ClientPassword" : "xxxx",         "TemporaryFolder" : "../../tmp"     } } )
EOF


docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.citoyens.dropIndexes();
db.cities.dropIndexes();
db.events.dropIndexes();
db.organizations.dropIndexes();
db.projects.dropIndexes();
EOF


docker-compose -f docker-compose-apache.yml exec -T mongo mongo mongo/pixelhumain <<EOF
db.citoyens.createIndex({"email": 1} , { unique: true });
db.cities.createIndex({"geoPosition": "2dsphere"});
db.cities.createIndex({"postalCodes.geoPosition": "2dsphere"});
db.cities.createIndex({"geoShape" : "2dsphere" });
db.cities.createIndex({"insee" : 1});
db.cities.createIndex({"region" : 1});
db.cities.createIndex({"dep" : 1});
db.cities.createIndex({"cp" : 1});
db.cities.createIndex({"country" : 1});
db.cities.createIndex({"postalCodes.name" : 1});
db.cities.createIndex({"postalCodes.postalCode" : 1});
db.events.createIndex({"geoPosition" : "2dsphere" });
db.events.createIndex({"parentId" : 1});
db.events.createIndex({"name" : 1});
db.organizations.createIndex({"geoPosition" : "2dsphere" });
db.projects.createIndex({"geoPosition" : "2dsphere" });
db.citoyens.createIndex({"geoPosition" : "2dsphere" });
EOF
#!/bin/sh
set -e

TAG="latest"
# TAG="testing"

cd docker-front-apache/
# docker build --no-cache --rm -t communecter/co2-front-dev:$TAG .
docker build --rm -t communecter/co2-front-dev:$TAG .
docker tag communecter/co2-front-dev:$TAG communecter/co2-front-dev:$TAG
docker push communecter/co2-front-dev:$TAG
